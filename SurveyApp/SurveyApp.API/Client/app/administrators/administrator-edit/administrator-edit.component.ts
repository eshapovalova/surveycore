import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Entity, Address, Administrator, AdministratorService } from '../../shared';

@Component({
  selector: 'app-administrator-edit',
  templateUrl: './administrator-edit.component.html',
  styleUrls: ['./administrator-edit.component.scss'],
  providers: [AdministratorService]
})
export class AdministratorEditComponent implements OnInit {

  // fields
  entity: Administrator = new Administrator();
  title: string = "New administrator";
  entityId: string;

  constructor(
    private _activateRoute: ActivatedRoute, 
    private _router: Router, 
    private _service: AdministratorService
  ) { }

  // events

  ngOnInit() {
    this.setEntity();
  }

  onSubmit(): void {
    console.log(this.entity);

    if(!!this.entityId){
      this._service.update(this.entityId, this.entity).then(() => this.cancel());
    } else {
      this._service.create(this.entity).then(() => this.cancel());
    }
  }

  // public methods:

  cancel(): void{
    this._router.navigate(['administrators']);
  }

  // private methods: 

  private setEntity(): void {
    this._activateRoute.params.subscribe((params: Params) => {
      this.entityId = params['id'];
      
      if(!!this.entityId){
        this._service.getById(this.entityId).subscribe(entity => {
          this.title = `Administrator: ${entity.firstName} ${entity.lastName}`;

          this.entity = entity;
        })
      } 
    })
  } 

}
