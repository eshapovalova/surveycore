import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdministratorsComponent } from './administrators.component';
import { AdministratorListComponent } from './administrator-list/administrator-list.component';
import { AdministratorEditComponent } from './administrator-edit/administrator-edit.component';
 
const routes: Routes = [
  { path: '', component: AdministratorsComponent, children: [
    { path: '', component: AdministratorListComponent },
    { path: 'edit/:id', component: AdministratorEditComponent },
    { path: 'new', component: AdministratorEditComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministratorsRoutingModule { }
