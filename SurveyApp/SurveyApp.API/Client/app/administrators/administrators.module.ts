import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ngx-bootstrap';

import { AdminHeaderComponent, AdminSidebarComponent } from './../shared'

import { AdministratorsRoutingModule } from './administrators-routing.module';
import { AdministratorsComponent } from './administrators.component';
import { AdministratorListComponent } from './administrator-list/administrator-list.component';
import { AdministratorEditComponent } from './administrator-edit/administrator-edit.component';

@NgModule({
  imports: [
    CommonModule,
    NgbDropdownModule.forRoot(),
    TranslateModule,
    Ng2TableModule,
    FormsModule,
    ReactiveFormsModule,
    AdministratorsRoutingModule,
    PaginationModule
  ],
  declarations: [
    AdministratorsComponent, 
    AdministratorListComponent, 
    AdministratorEditComponent,
    AdminHeaderComponent,
    AdminSidebarComponent
  ]
})
export class AdministratorsModule { }
