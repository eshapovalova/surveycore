﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared';

const routes: Routes = [
    { path: '', loadChildren: './surveys/surveys.module#SurveysModule' },
    { path: 'organisations', loadChildren: './organisations/organisations.module#OrganisationsModule' },
    { path: 'administrators', loadChildren: './administrators/administrators.module#AdministratorsModule' },
    { path: 'participants', loadChildren: './participants/participants.module#ParticipantsModule' },
    {
        path: 'layout',
        loadChildren: './layout/layout.module#LayoutModule',
        canActivate: [AuthGuard]
    },
    { path: 'login', loadChildren: './login/login.module#LoginModule' },
    { path: 'signup', loadChildren: './signup/signup.module#SignupModule' },
    { path: 'not-found', loadChildren: './not-found/not-found.module#NotFoundModule' },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
