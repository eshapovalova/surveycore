import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Entity, Address, Organisation, OrganisationService } from '../../shared/';

@Component({
  selector: 'app-organisation-edit',
  templateUrl: './organisation-edit.component.html',
  styleUrls: ['./organisation-edit.component.scss'],
  providers: [OrganisationService]
})
export class OrganisationEditComponent implements OnInit {

  // fields
  entity: Organisation = new Organisation();  
  title: string = "New organisation";
  entityId: string;

  constructor(
    private activatedRoute: ActivatedRoute, 
    private route: Router, 
    private service: OrganisationService
  ) { }

  ngOnInit() {
    this.setEntity();
  }  

  onSubmit(): void {
    if(!!this.entityId){
      this.service.update(this.entityId, this.entity).then(() => this.cancel());
    } else {
      this.service.create(this.entity).then(() => this.cancel());
    }
  }

  // public methods: 

  cancel(): void {
    this.route.navigate(['organisations']);
  }

  // private methods:

  private setEntity(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.entityId = params['id'];

      if(!!this.entityId){
        this.service.getById(this.entityId).subscribe(entity => {
          this.title = `Organisation: ${entity.legalName}`;

          this.entity = entity;
        })
      }
    })
  }
}
