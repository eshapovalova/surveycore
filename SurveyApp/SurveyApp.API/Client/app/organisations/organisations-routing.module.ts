import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrganisationsComponent } from './organisations.component';
import { OrganisationListComponent } from './organisation-list/organisation-list.component';
import { OrganisationEditComponent } from './organisation-edit/organisation-edit.component';

const routes: Routes = [
  { path: '', component: OrganisationsComponent, children: [
    { path: '', component: OrganisationListComponent },
    { path: 'edit/:id', component: OrganisationEditComponent },
    { path: 'new', component: OrganisationEditComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrganisationsRoutingModule { }
