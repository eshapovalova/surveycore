import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ngx-bootstrap';

import { OrgHeaderComponent, OrgSidebarComponent } from './../shared';

import { OrganisationsRoutingModule } from './organisations-routing.module';
import { OrganisationsComponent } from './organisations.component';
import { OrganisationListComponent } from './organisation-list/organisation-list.component';
import { OrganisationEditComponent } from './organisation-edit/organisation-edit.component';

@NgModule({
  imports: [
    CommonModule,
    NgbDropdownModule.forRoot(),
    PaginationModule,
    TranslateModule,
    OrganisationsRoutingModule,
    Ng2TableModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    OrganisationsComponent, 
    OrganisationListComponent, 
    OrganisationEditComponent, 
    OrgHeaderComponent, 
    OrgSidebarComponent]
})
export class OrganisationsModule { }
