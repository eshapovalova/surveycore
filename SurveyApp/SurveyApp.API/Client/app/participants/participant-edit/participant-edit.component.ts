import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Entity, Address, Participant, ParticipantService } from '../../shared'

@Component({
  selector: 'app-participant-edit',
  templateUrl: './participant-edit.component.html',
  styleUrls: ['./participant-edit.component.scss'],
  providers: [ParticipantService]
})
export class ParticipantEditComponent implements OnInit {

  // =============================================================================
  // Fields
  // =============================================================================

  entity: Participant = new Participant();
  title: string = "New user";
  entityId: string;

  constructor(
    private _activateRoute: ActivatedRoute,
    private _router: Router,
    private _service: ParticipantService
  ) { }

  ngOnInit() {
    this.setEntity();
  }

  onSubmit(): void {
    console.log(this.entity);

    if(!!this.entityId){
      this._service.update(this.entityId, this.entity).then(() => this.cancel());
    } else {
      this._service.create(this.entity).then(() => this.cancel());
    }
  }

  cancel(): void{
    this._router.navigate(['participants']);
  }

  private setEntity(): void {
    this._activateRoute.params.subscribe((params: Params) => {
      this.entityId = params['id'];
      
      if(!!this.entityId){
        this._service.getById(this.entityId).subscribe(entity => {
          this.title = `${entity.firstName} ${entity.lastName}`;

          this.entity = entity;
        })
      } 
    })
  } 

}
