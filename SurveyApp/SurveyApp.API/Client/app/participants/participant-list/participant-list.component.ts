import { Component, OnInit, OnChanges, ViewEncapsulation, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { PaginationConfig, PaginationComponent } from 'ngx-bootstrap';

import { Entity, Address, Participant, ParticipantService } from '../../shared/';

@Component({
  selector: 'app-participant-list',
  templateUrl: './participant-list.component.html',
  styleUrls: ['./participant-list.component.scss'],
  providers: [ParticipantService,PaginationConfig],
  encapsulation: ViewEncapsulation.None
})
export class ParticipantListComponent implements OnInit {

  rows: Array<any> = [];
  dataFromService: Array<any> = [];

  columns = [
    { name: 'actionEdit', title: '', sort: false, className: 'accepter-col-action' },    
    { name: 'actionDelete', title: '', sort: false, className: 'accepter-col-action' },  
    { name: 'firstName', title: 'First Name', filtering: { filterString: '', placeholder: 'Filter by first name'} },
    { name: 'lastName', title: 'Last Name', filtering: { filterString: '', placeholder: 'Filter by last name'} },
    { name: 'phone', title: 'Phone', filtering: { filterString: '', placeholder: 'Filter by phone'} },
    { name: 'email', title: 'Email', filtering: { filterString: '', placeholder: 'Filter by email'} }
  ]

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public numPages: number = 1;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: { columns: this.columns },
    filtering: { filterString: '' },
    className: ['table-striped', 'table-bordered', 'table-condensed']
  };

  constructor(private _router: Router, private _service: ParticipantService) { }

  ngOnInit() {
    this.getAll();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(this.rows != null){
      this.extendData();      
    }
  }

  // ===========================================================================
  // Public methods
  // ===========================================================================

  create(): void {
    this._router.navigate(['participants/new']);
  }

  onCellClick(data: any): any {
    if(data.column === "actionEdit"){
      this._router.navigate(['participants/edit', data.row.idStr]);
    }

    if(data.column === "actionDelete"){
      if(confirm('Are you sure you want to delete this item?')){
        this._service.delete(data.row.idStr).subscribe(res =>{
          console.log(res);
          if(res === true){
            alert('The item has been successfull deleted');
            this.getAll();
          }            
        })
      }
    }
  }

  public changePage(page:any, data:Array<any> = this.rows):Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }
  public changeSort(data:any, config:any):any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName:string = void 0;
    let sort:string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous:any, current:any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data:any, config:any):any {
    let filteredData:Array<any> = this.dataFromService;
    this.columns.forEach((column:any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item:any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item:any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray:Array<any> = [];
    filteredData.forEach((item:any) => {
      let flag = false;
      this.columns.forEach((column:any) => {
        let value = item[column.name];

        if(!!value){
          if (value.toString().match(this.config.filtering.filterString)) {
            flag = true;
          }
        }       
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.rows, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  // ===========================================================================
  // Private methods
  // ===========================================================================

  private getAll(){
    this._service.getAll().subscribe(res => {
      this.rows = res;
      this.dataFromService = res;

      this.extendData();
      this.onChangeTable(this.config);
    })
  }

  private extendData() {
    for(let i = 0; i < this.rows.length; i++){
      
      var obj = this.rows[i];

      obj.actionEdit = '<a class="action-btn btn btn-success btn-sm edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';
      obj.actionDelete = '<a class="action-btn btn btn-danger btn-sm remove"><i class="fa fa-trash-o" aria-hidden="true"></i></a>';     
    }
  }
}
