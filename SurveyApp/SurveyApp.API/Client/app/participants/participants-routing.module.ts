import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParticipantsComponent } from './participants.component';
import { ParticipantListComponent } from './participant-list/participant-list.component';
import { ParticipantEditComponent } from './participant-edit/participant-edit.component';

const routes: Routes = [
  { path: '', component: ParticipantsComponent, children: [
    { path: '', component: ParticipantListComponent },
    { path: 'edit/:id', component: ParticipantEditComponent },
    { path: 'new', component: ParticipantEditComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParticipantsRoutingModule { }
