import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';

import { Ng2TableModule } from 'ng2-table/ng2-table';
import { PaginationModule } from 'ngx-bootstrap';

import { UserHeaderComponent, UserSidebarComponent } from './../shared'

import { ParticipantsRoutingModule } from './participants-routing.module';
import { ParticipantsComponent } from './participants.component';
import { ParticipantListComponent } from './participant-list/participant-list.component';
import { ParticipantEditComponent } from './participant-edit/participant-edit.component';

@NgModule({
  imports: [
    CommonModule,
    NgbDropdownModule.forRoot(),
    TranslateModule,
    Ng2TableModule,
    FormsModule,
    ReactiveFormsModule,
    ParticipantsRoutingModule,
    PaginationModule
  ],
  declarations: [
    ParticipantsComponent, 
    ParticipantListComponent, 
    ParticipantEditComponent,
    UserHeaderComponent,
    UserSidebarComponent
  ]
})
export class ParticipantsModule { }
