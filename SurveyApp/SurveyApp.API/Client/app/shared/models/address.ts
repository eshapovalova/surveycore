﻿export class Address {
    public addressType: string;
    public addressLine1: string;
    public addressLine2: string;
    public addressLine3: string;
    public city: string;
    public postCode: string;
    public state: string;
    public country: string;
}