﻿import { Entity } from './entity';
import { Address } from './address';


export class Administrator extends Entity {
    public key: string;
    public organisationKey: string;
    public firstName: string;
    public lastName: string;
    public address: Address;
    public phone: string;
    public email: string;
}
