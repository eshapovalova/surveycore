﻿export class Entity {
    public createdAt: Date;
    public updatedAt: Date;
    public createdBy: string;
    public updatedBy: string;
    public isDeleted: boolean;

    public idStr: string;
}
