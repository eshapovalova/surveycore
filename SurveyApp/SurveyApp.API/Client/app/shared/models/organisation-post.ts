import { Address } from './address';

export class OrganisationPost {
    key: string;
    legalName: string;
    address: Address
    phone: string;
    email: string;
}
