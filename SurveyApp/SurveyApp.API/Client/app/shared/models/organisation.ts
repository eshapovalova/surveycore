﻿import { Entity } from './entity';
import { Address } from './address';

export class Organisation extends Entity {
    constructor(){
        super();
        this.address = new Address();
    }

    public key: string;
    public legalName: string;
    public address: Address;
    public phone: string;
    public email: string;
}
