import { Entity } from './entity';
import { Address } from './address';

export class Participant extends Entity {

    key: string;
    identityKey: string;
    clientKey: string;
    firstName: string;
    lastName: string;
    address: Address;
    phone: string;
    email: string;
}
