import { Entity } from './entity';
import { SurveyPage } from './survey-page';

export class SurveyDetail extends Entity {

    organisationKey: string;
    surveyKey: string;
    surveyStatus: string;
    releasedDate: Date;
    completionDate: Date;

    participantKeys: string[];
    surveyComments: string;
    surveyTemplateVersion: string;

    title: string;
    pages: SurveyPage[];
}
