﻿
export enum SurveyElementType {
    text,
    radiogroup,
    panel,
    multipletext,
    matrixdynamic,
    matrixdropdown,
    matrix,
    file,
    html,
    Rating,
    dropdown,
    comment,
    checkbox
}
