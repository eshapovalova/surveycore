﻿import { SurveyItem } from './survey-item';

export class SurveyElement {
    public Type: string;
    public Name: string;
    public Title: string;
    public IsRequired: boolean;
    public Choices: Object[];
    public Items: SurveyItem[];
    public Columns: Object;
    public Rows: string[];
    public Visible: boolean;
    public VisibleIf: string;
    public HasOther: boolean;
}
