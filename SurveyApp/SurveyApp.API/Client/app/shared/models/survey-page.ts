﻿import { SurveyElement } from './survey-element';

export class SurveyPage {
    public Elements: SurveyElement[];
    public Name: string;
}
