﻿import { SurveyPage } from './survey-page';
import { Entity } from './entity';

export class SurveyResponse extends Entity {    

    public surveyResponseKey: string;
    public surveyKey: string;
    public participantKey: string;
    public progress: string;
    public openedDate: Date;
    public completionDate: Date;
    public response: string;
    public responseComments: string;
    public title: string;
    public pages: SurveyPage[];
}
