import { Entity } from './entity';
import { SurveyPage } from './survey-page';

export class SurveyTemplate extends Entity {

    title: string;
    organisationKey: string;
    status: string;
    version: string;

    public pages: SurveyPage[];
}
