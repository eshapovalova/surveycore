﻿import { SurveyPage } from './survey-page';

export class SurveyModel {

    constructor(title: string) {
        this.title = title;

        console.log('Survey with name: ' + title);
    }

    //json for pages;
    public pages: SurveyPage[];

    public title: string;
}
