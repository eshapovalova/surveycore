import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { Entity, Address, Participant } from '../models/'

@Injectable()
export class ParticipantService {
  private serviceUrl = 'api/participants';

  headers: Headers;

  constructor(private http: Http) {
    this.headers = new Headers( { 'Content-Type': 'application/json' });
  }

  getAll(): Observable<Participant[]>{
    return this.http.get(this.serviceUrl).map((res: Response) => res.json()).catch(this.handleError);
  }

  getById(id: string): Observable<Participant>{
    const url = `${this.serviceUrl}/${id}`;

    return this.http.get(url).map((res: Response) => res.json() as Participant).catch(this.handleError);
  }

  create(entity: Participant){
    return this.http.post(this.serviceUrl, entity, { headers: this.headers })
    .toPromise()
    .then(res=> 
    {
      console.log('create response: ' + res)
    })
    .catch(this.handleError);
  }

  update(id: string, entity: Participant) {
    const url = `${this.serviceUrl}/${id}`;
    
    return this.http.put(url, entity, { headers: this.headers}).toPromise().then(res => {
      console.log('update response' + res);
    }).catch(this.handleError);
  }

  delete(id: string): Observable<boolean>{
    const url = `${this.serviceUrl}/${id}`;

    return this.http.delete(url, { headers: this.headers}).map((res: Response) => res.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
