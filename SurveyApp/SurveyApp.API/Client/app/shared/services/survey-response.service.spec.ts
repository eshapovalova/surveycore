import { TestBed, inject } from '@angular/core/testing';

import { SurveyResponseService } from './survey-response.service';

describe('SurveyResponseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SurveyResponseService]
    });
  });

  it('should be created', inject([SurveyResponseService], (service: SurveyResponseService) => {
    expect(service).toBeTruthy();
  }));
});
