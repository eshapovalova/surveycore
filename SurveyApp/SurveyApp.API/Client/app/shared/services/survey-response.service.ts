import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { SurveyDetail, SurveyResponse } from '../models';

@Injectable()
export class SurveyResponseService {

  private serviceUrl = 'api/SurveyResponse';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  // get all results 
  getAll(): Observable<SurveyResponse[]> {
    return this.http.get(this.serviceUrl).map((res: Response) => res.json()).catch(this.handleError);
  }

  getById(id: string): Observable<SurveyResponse> {
    const url = `${this.serviceUrl}/${id}`;

    return this.http.get(url).map((res: Response) => res.json() as SurveyResponse).catch(this.handleError);
  }

  getUserResponses(id: string): Observable<SurveyResponse[]> {
    const url = `${this.serviceUrl}/user/${id}`;

    return this.http.get(url).map((res: Response) => res.json() as SurveyResponse).catch(this.handleError);
  }

  saveResponse(entity: SurveyResponse) {
    return this.http.post(this.serviceUrl, entity, { headers: this.headers})
    .toPromise()
    .then(res => res)
    .catch(this.handleError);
  }

  updateResponse(id: string, model: SurveyResponse): Promise<any> {
    const url = `${this.serviceUrl}/${id}`;
    
    return this.http.put(url, model, { headers: this.headers }).toPromise().then(res => res).catch(this.handleError);
  }

  updateUserResponse(id: string, model: any): Promise<any> {
    const url = `${this.serviceUrl}/fill/${id}`;
    
    return this.http.put(url, model, { headers: this.headers }).toPromise().then(res => res).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }

}
