import { TestBed, inject } from '@angular/core/testing';

import { SurveyTemplateService } from './survey-template.service';

describe('SurveyTemplateService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SurveyTemplateService]
    });
  });

  it('should be created', inject([SurveyTemplateService], (service: SurveyTemplateService) => {
    expect(service).toBeTruthy();
  }));
});
