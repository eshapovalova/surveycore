﻿import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { SurveyTemplate } from '../models/';

@Injectable()
export class SurveyTemplateService {

  private serviceUrl = 'api/templates';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  getAll(): Observable<SurveyTemplate[]> {
    return this.http.get(this.serviceUrl)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  getById(id: string): Promise<SurveyTemplate> {
    const url = `${this.serviceUrl}/${id}`;

    return this.http.get(url).toPromise().then(response => response.json() as SurveyTemplate).catch(this.handleError);
  }

  create(model: SurveyTemplate): Promise<any> {
    return this.http
      .post(this.serviceUrl, model, { headers: this.headers })
      .toPromise()
      .then(res => res)
      .catch(this.handleError);
  }

  update(id: string, model: SurveyTemplate): Promise<any> {
    const url = `${this.serviceUrl}/${id}`;

    return this.http.put(url, model, { headers: this.headers }).toPromise().then(res => res).catch(this.handleError);
  }
  
  delete(id: string): Observable<boolean>{
    const url = `${this.serviceUrl}/${id}`;

    return this.http.delete(url, { headers: this.headers}).map((res: Response) => res.json()).catch(this.handleError);
  }

  useTemplate(id: string): Observable<Response>{
    let copyUrl = `${this.serviceUrl}/use/${id}`;

    return this.http.post(copyUrl, { headers: this.headers }).map((res: Response) => res.text()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}