﻿import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { SurveyDetail, SurveyModel } from '../models/';

@Injectable()
export class SurveyService {   

  private serviceUrl = 'api/surveys';
  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private http: Http) { }

  // surveys

  getAll(): Observable<SurveyDetail[]>{
    return this.http.get(this.serviceUrl)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }  

  getById(id: string): Observable<SurveyDetail>{
    const url = `${this.serviceUrl}/${id}`;
    return this.http.get(url).map((res: Response) => res.json() as SurveyDetail).catch(this.handleError);
  }  

  update(id: string, entity: SurveyDetail) {
    const url = `${this.serviceUrl}/${id}`;

    return this.http.put(url, entity, {headers: this.headers}).toPromise().then(res => res).catch(this.handleError);
  }

  updateSurveyPages(id: string, entity: SurveyModel) {
    const url = `${this.serviceUrl}/designer/${id}`;

    return this.http.put(url, entity, { headers: this.headers }).toPromise().then(res => res).catch(this.handleError);
  }

  delete(id: string): Promise<any> {
    return this.http.post(this.serviceUrl, id, { headers: this.headers }).toPromise().then(res => res).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
