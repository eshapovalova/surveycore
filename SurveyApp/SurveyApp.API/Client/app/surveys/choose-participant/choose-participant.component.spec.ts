import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooseParticipantComponent } from './choose-participant.component';

describe('ChooseParticipantComponent', () => {
  let component: ChooseParticipantComponent;
  let fixture: ComponentFixture<ChooseParticipantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseParticipantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooseParticipantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
