import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { Location } from '@angular/common';

import { Participant, ParticipantService } from '../../shared';

@Component({
  selector: 'app-choose-participant',
  templateUrl: './choose-participant.component.html',
  styleUrls: ['./choose-participant.component.scss'],
  providers: [ParticipantService]
})
export class ChooseParticipantComponent implements OnInit {

  participants: Participant[];
  selectedKey: string;
  surveyKey: string;

  constructor(
    private _service: ParticipantService, 
    private _router: Router,
    private _activatedRoute: ActivatedRoute, 
    private _location: Location
  ) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe((params: Params) => {
      this.surveyKey = params['id'];
    })

    this._service.getAll().subscribe(items => {
      this.participants = items;
    })
  }

  choose() {
    if(!!this.selectedKey){
      this._router.navigate(['surveys', this.surveyKey, this.selectedKey]);
    }
  }
}
