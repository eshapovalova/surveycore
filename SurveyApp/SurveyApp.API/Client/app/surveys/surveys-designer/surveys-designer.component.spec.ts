import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysDesignerComponent } from './surveys-designer.component';

describe('SurveysDesignerComponent', () => {
  let component: SurveysDesignerComponent;
  let fixture: ComponentFixture<SurveysDesignerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysDesignerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysDesignerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
