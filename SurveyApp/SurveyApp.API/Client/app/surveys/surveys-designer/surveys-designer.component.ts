import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, ParamMap, Params } from '@angular/router';
import { Location } from '@angular/common';

import * as SurveyEditor from 'surveyjs-editor';

import 'rxjs/add/operator/switchMap';

import { SurveyModel, SurveyDetail, SurveyService } from '../../shared/'

@Component({
  selector: 'app-surveys-designer',
  templateUrl: './surveys-designer.component.html',
  styleUrls: ['./surveys-designer.component.scss'],
  providers: [SurveyService]
})
export class SurveysDesignerComponent implements OnInit {

  editor: SurveyEditor.SurveyEditor;
  surveyId: string;
  title: string = "Survey Designer";
  
  constructor(
    private _activatedRoute: ActivatedRoute, 
    private _location: Location,
    private _service: SurveyService
  ) { }

  ngOnInit() {
    let editorOptions = 
    { 
      showEmbededSurveyTab: false, 
      showOptions: false, 
      showJSONEditorTab: false, 
      generateValidJSON: true, 
      showTestSurveyTab: false 
    };
    
    this.editor = new SurveyEditor.SurveyEditor('surveyEditorContainer', editorOptions);       

    this._activatedRoute.params.subscribe((params: Params) => {
      let id = params['id'];

      if(!!id){
        this.surveyId = id;

        this._service.getById(id).subscribe(res => {
          this.title = res.title;
          this.editor.text = JSON.stringify(res);          
        })
      }
    });    

    this.editor.saveSurveyFunc = this.save;    
  }

  private save= () => {
    debugger;
    if(!!this.surveyId) {
      var surveyTemplateObj = JSON.parse(this.editor.text) as SurveyModel;

      this._service.updateSurveyPages(this.surveyId, surveyTemplateObj).then(() => {
        this.goBack();
      })
    }
  }

  private goBack(): void {
    this._location.back();
  }
}
