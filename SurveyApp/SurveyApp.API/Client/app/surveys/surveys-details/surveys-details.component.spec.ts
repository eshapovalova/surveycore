import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysDetailsComponent } from './surveys-details.component';

describe('SurveysDetailsComponent', () => {
  let component: SurveysDetailsComponent;
  let fixture: ComponentFixture<SurveysDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
