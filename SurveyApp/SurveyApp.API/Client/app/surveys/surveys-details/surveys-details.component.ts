import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Entity, SurveyDetail, Participant, SurveyService, ParticipantService } from '../../shared'

@Component({
  selector: 'app-surveys-details',
  templateUrl: './surveys-details.component.html',
  styleUrls: ['./surveys-details.component.scss'],
  providers: [SurveyService, ParticipantService]
})
export class SurveysDetailsComponent implements OnInit {

  entity: SurveyDetail = new SurveyDetail();
  title: string = "Survey details";
  entityId: string;
  releasedDateModel: Date;
  allParticipants: Array<any> = [];
  surveyParticipants: Array<any> = [];

  statuses: Array<string> = [
    "Created",
    "Published",
    "Recalled",
    "Completed"
  ]

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _service: SurveyService,
    private _participantService: ParticipantService
  ) { }

  // ===========================================================================
  // Events
  // ===========================================================================

  ngOnInit() {
    this.setEntity();    
  }

  onSubmit(): void {
    if(!!this.entityId){
      this._service.update(this.entityId, this.entity).then(() => {
        this._router.navigate(['surveys']);
      })
    }
  }

  cancel(): void{
    this._router.navigate(['surveys']);
  }

  goToDesigner(): void{
    if(confirm('The changes were not saved. Do you want to save now?')){
      this._service.update(this.entityId, this.entity).then(() => {        
        this._router.navigate(['designer', this.entityId]);
      })
    } else{
      this._router.navigate(['designer', this.entityId]);
    }
  }

  copyToParticipant(user: Participant){
    let isExist: boolean = false;

    if(!!this.surveyParticipants && this.surveyParticipants.length > 0){
      this.surveyParticipants.forEach((item: Participant) => {
        if(item.idStr === user.idStr){
          isExist = true;
        }
      });
    }

    if(isExist === false){
      this.surveyParticipants.push(user);

      if(this.entity.participantKeys === null){
        this.entity.participantKeys = [];
      }

      this.entity.participantKeys.push(user.idStr);
    }
  }

  removeParticipant(user: Participant){
    if(this.surveyParticipants.length > 0){
      this.surveyParticipants = this.surveyParticipants.filter((item: Participant) => {
        return item.idStr !== user.idStr;
      })
    }

    if(this.entity.participantKeys.length > 0){
      this.entity.participantKeys = this.entity.participantKeys.filter(id => {
        return id !== user.idStr;
      })
    }
  }

  // ===========================================================================
  // Private methods
  // ===========================================================================

  private setEntity(): void{
    this._activatedRoute.params.subscribe((params: Params) => {
      this.entityId = params['id'];

      if(!!this.entityId){
        this._service.getById(this.entityId).subscribe(entity => {
          this.entity = entity;

          this.releasedDateModel = new Date(entity.releasedDate);

          this.loadParticipants();
        })  
      }
    })
  }

  private loadParticipants(): void {
    this._participantService.getAll().subscribe(res => {
      this.allParticipants = res;

      if(!!this.entity.participantKeys){
        this.allParticipants.forEach((user: Participant) => {
          if(this.entity.participantKeys.includes(user.idStr)){
            this.surveyParticipants.push(user);
          }
        })
      }
    });
  }
}
