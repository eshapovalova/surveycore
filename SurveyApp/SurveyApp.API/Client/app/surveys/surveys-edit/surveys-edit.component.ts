import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { Location } from '@angular/common';
import * as Survey from 'survey-angular';

import 'rxjs/add/operator/switchMap';

import { SurveyDetail, SurveyService } from '../../shared/';

Survey.Survey.cssType = "bootstrap";
Survey.defaultBootstrapCss.navigationButton = "btn btn-success";

@Component({
  selector: 'app-surveys-edit',
  templateUrl: './surveys-edit.component.html',
  styleUrls: ['./surveys-edit.component.scss'],
  providers: [SurveyService]
})
export class SurveysEditComponent implements OnInit {
  previewSurvey: SurveyDetail;
  surveyJson: string;
  surveyId: string;

  constructor(private service: SurveyService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {

    this.route.params.subscribe((params: Params) => {
      this.surveyId = params['id'];

      if(!!this.surveyId){
        this.service.getById(this.surveyId).subscribe(survey => {
          this.previewSurvey = survey;

          let surveyModel = new Survey.ReactSurveyModel(this.previewSurvey);

          surveyModel.onComplete.add((result: any) => {
            this.complete(result);
          });

          Survey.SurveyNG.render('surveyElement', { model: surveyModel });
        })
      }
    });
  }

  complete(result: any) {
    console.log("result: " + JSON.stringify(result.data));

    // this.service(this.surveyId, result.data).then(res => {
    //   console.log(res);
    // })    
  }
}
