import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysInstanceComponent } from './surveys-instance.component';

describe('SurveysInstanceComponent', () => {
  let component: SurveysInstanceComponent;
  let fixture: ComponentFixture<SurveysInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
