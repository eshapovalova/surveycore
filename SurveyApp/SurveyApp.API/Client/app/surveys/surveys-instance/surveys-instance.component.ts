import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { Location } from '@angular/common';
import * as Survey from 'survey-angular';

import 'rxjs/add/operator/switchMap';

import { SurveyResponse, SurveyDetail, SurveyService, SurveyResponseService } from '../../shared'

Survey.Survey.cssType = "bootstrap";
Survey.defaultBootstrapCss.navigationButton = "btn btn-success";

@Component({
  selector: 'app-surveys-instance',
  templateUrl: './surveys-instance.component.html',
  styleUrls: ['./surveys-instance.component.scss'],
  providers: [SurveyService, SurveyResponseService]
})
export class SurveysInstanceComponent implements OnInit {
  surveyInstance:SurveyDetail = new SurveyDetail();
  surveyJson: string;
  surveyKey: string;
  participantKey: string;
  responseKey: string;
  surveyModel: Survey.ReactSurveyModel;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _surveyService: SurveyService,
    private _responseService: SurveyResponseService
  ) 
  { }

  ngOnInit() {

    this._activatedRoute.params.subscribe((params: Params) => {
      this.surveyKey = params['id'];
      this.participantKey = params['participant'];
      this.responseKey = params["responseId"];      

      if (!!this.responseKey) {
        this._responseService.getById(this.responseKey).subscribe(response => {
          this.surveyInstance.title = response.title
          this.surveyInstance.pages = response.pages;
          this.surveyModel = new Survey.ReactSurveyModel(this.surveyInstance);

          Survey.SurveyNG.render('surveyElement', { model: this.surveyModel, data: JSON.parse(response.response) });

          this.surveyModel.onComplete.add((result: any) => {
            this.complete(result);
          });
        })
      } else {
        this._surveyService.getById(this.surveyKey).subscribe(surveyInfo => {          
          this.surveyInstance.title = surveyInfo.title
          this.surveyInstance.pages = surveyInfo.pages;

          this.surveyModel = new Survey.ReactSurveyModel(this.surveyInstance);

          Survey.SurveyNG.render('surveyElement', { model: this.surveyModel });

          this.surveyModel.onComplete.add((result: any) => {
            this.complete(result);
          });
        })
      }
    });
  }

  complete(result: any) {
    console.log(result);

    if(!!this.responseKey) {
      var newData = JSON.stringify(result.data);

      this._responseService.updateUserResponse(this.responseKey, newData).then(res => {
        // navigate to ???
        alert('changes has been saved');
      })
    } else{
      let responseModel = new SurveyResponse();
      responseModel.participantKey = this.participantKey;
      responseModel.surveyKey = this.surveyKey;
      responseModel.progress = this.surveyModel.getProgress().toString();
      responseModel.response = JSON.stringify(result.data);

      this._responseService.saveResponse(responseModel).then(res => {
        if(!!res){
          // navigate to ??? 
          alert('changes has been saved');
        }
      })
    }
  }

}
