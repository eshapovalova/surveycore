import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysResponseDetailsComponent } from './surveys-response-details.component';

describe('SurveysResponseDetailsComponent', () => {
  let component: SurveysResponseDetailsComponent;
  let fixture: ComponentFixture<SurveysResponseDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysResponseDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysResponseDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
