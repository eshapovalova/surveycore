import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Entity, SurveyResponse, SurveyResponseService } from '../../shared';


@Component({
  selector: 'app-surveys-response-details',
  templateUrl: './surveys-response-details.component.html',
  styleUrls: ['./surveys-response-details.component.scss'],
  providers: [SurveyResponseService]
})
export class SurveysResponseDetailsComponent implements OnInit {

  entity: SurveyResponse = new SurveyResponse();
  title: string = "Survey response details";
  entityId: string;
  textToCopy: string;
  isCopied: boolean;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _service: SurveyResponseService
  ) { }

  // ===========================================================================
  // Events
  // ===========================================================================  

  ngOnInit() {
    this.setEntity();
  }

  onSubmit(): void {
    if (!!this.entityId) {
      this._service.updateResponse(this.entityId, this.entity).then(() => {
        this._router.navigate(['responses']);
      })
    }
  }

  cancel(): void {
    this._router.navigate(['responses']);
  }

  showResponse(): void {
    this._router.navigate(['surveys', this.entity.surveyKey, this.entity.participantKey, this.entity.idStr]);
  }

  // =============================================================================
  //  Private methods
  // =============================================================================

  private setEntity(): void { 
    this._activatedRoute.params.subscribe((params: Params) => {
      this.entityId = params['id'];

      if(!!this.entityId){
        this._service.getById(this.entityId).subscribe(entity => { 
          this.entity = entity;
          this.title = entity.title;
          this.entity.surveyResponseKey = entity.idStr;
          this.textToCopy = `${window.location.origin}/surveys/${this.entity.surveyKey}/${this.entity.participantKey}/${this.entity.idStr}`;
        })
      }
    })
  }
}
