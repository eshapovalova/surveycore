import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysResponseComponent } from './surveys-response.component';

describe('SurveysResponseComponent', () => {
  let component: SurveysResponseComponent;
  let fixture: ComponentFixture<SurveysResponseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysResponseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysResponseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
