import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';

import { SurveyResponse, Participant, SurveyResponseService, ParticipantService } from '../../shared/';

@Component({
  selector: 'app-surveys-response',
  templateUrl: './surveys-response.component.html',
  styleUrls: ['./surveys-response.component.scss'],
  providers: [SurveyResponseService,ParticipantService],
  encapsulation:ViewEncapsulation.None
})
export class SurveysResponseComponent implements OnInit {
 
  public items: Array<any> = [];
  public selectedKey: string;
  public participants: Participant[];
  
  columns = [
    { name: 'actionContinue', title: '', sort: false, className: 'accepter-col-action'},
    { name: 'actionEdit', title: '', sort: false, className: 'accepter-col-action' },
    { name: 'surveyKey', title: 'Survey Key' },
    { name: 'title', title: 'title' },
    { name: 'progress', title: 'progress' },
    { name: 'openedDate', title: 'openedDate' },
    { name: 'completionDate', title: 'Completion Date' },
    { name: 'version', title: 'Version' }
  ]

  public config: any = {
    className: ['table-striped', 'table-bordered', 'table-condensed']
  };

  constructor(private _router: Router, private _service: SurveyResponseService, private _participantService: ParticipantService) { }

  ngOnInit() {
    this._participantService.getAll().subscribe(response => {
      this.participants = response;
    })
  }

  onCellClick(data: any): any {
    console.log(data);

    if(data.column === "actionEdit"){
      this.edit(data.row.idStr);
    }

    if(data.column === "actionContinue"){
      var dataRow = <SurveyResponse>(data.row);

      if(!!dataRow){
        this._router.navigate(['surveys', dataRow.surveyKey, dataRow.participantKey, dataRow.idStr]);
      }
    }
  }

  choose() {
    if(!!this.selectedKey) {
      this._service.getUserResponses(this.selectedKey).subscribe(response => {
        this.items = response;

        this.extendData();
      })
    }
  }

  private edit(id: string) { 
    this._router.navigate(['responses/details', id]);
  }

  private extendData() {
    for(let i = 0; i < this.items.length; i++){
      
      var obj = this.items[i];

      obj.actionEdit = '<a class="action-btn btn btn-success btn-sm edit"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>';      
      obj.actionContinue = '<a class="action-btn btn btn-sm btn-primary"><i class="fa fa-play" aria-hidden="true"></i></a>';
    }
  }
}
