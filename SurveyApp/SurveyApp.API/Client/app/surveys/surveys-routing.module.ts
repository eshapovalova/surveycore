﻿import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurveysComponent } from './surveys.component';
import { SurveysListComponent } from './surveys-list/surveys-list.component';
import { SurveysEditComponent } from './surveys-edit/surveys-edit.component';
import { SurveysTemplatesComponent } from './surveys-templates/surveys-templates.component';
import { SurveysTemplatesEditComponent } from './surveys-templates-edit/surveys-templates-edit.component';
import { SurveysResponseComponent } from './surveys-response/surveys-response.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SurveysDetailsComponent } from './surveys-details/surveys-details.component';
import { SurveysDesignerComponent } from './surveys-designer/surveys-designer.component';
import { SurveysInstanceComponent } from './surveys-instance/surveys-instance.component';
import { ChooseParticipantComponent } from './choose-participant/choose-participant.component';
import { SurveysResponseDetailsComponent } from './surveys-response-details/surveys-response-details.component';

const routes: Routes = [{
    path: '', component: SurveysComponent, children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'surveys', component: SurveysListComponent },
        { path: 'surveys/:id', component: ChooseParticipantComponent},
        { path: 'surveys/:id/:participant', component: SurveysInstanceComponent },
        { path: 'surveys/:id/:participant/:responseId', component: SurveysInstanceComponent },
        { path: 'survey-templates', component: SurveysTemplatesComponent},
        { path: 'survey-templates/:id', component: SurveysTemplatesEditComponent },
        { path: 'survey-template', component: SurveysTemplatesEditComponent },
        { path: 'responses', component: SurveysResponseComponent},
        { path: 'responses/details/:id', component: SurveysResponseDetailsComponent },
        { path: 'designer/:id', component: SurveysDesignerComponent },
        { path: 'details/:id', component: SurveysDetailsComponent }
        
    ]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SurveysRoutingModule { }
