import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysTemplatesEditComponent } from './surveys-templates-edit.component';

describe('SurveysTemplatesEditComponent', () => {
  let component: SurveysTemplatesEditComponent;
  let fixture: ComponentFixture<SurveysTemplatesEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysTemplatesEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysTemplatesEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
