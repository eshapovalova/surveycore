import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { Location } from '@angular/common';

import * as SurveyEditor from 'surveyjs-editor';

import 'rxjs/add/operator/switchMap';

import { SurveyTemplate, SurveyTemplateService } from '../../shared/';

@Component({
  selector: 'app-surveys-templates-edit',
  templateUrl: './surveys-templates-edit.component.html',
  styleUrls: ['./surveys-templates-edit.component.scss'],
  providers: [SurveyTemplateService]
})

export class SurveysTemplatesEditComponent implements OnInit {

  editor: SurveyEditor.SurveyEditor;
  surveyTemplateText: string;
  surveyTemplateId: string;
  surveyTemplate: SurveyTemplate = new SurveyTemplate();
  title: string = "Survey Template";

  statuses: Array<string> = [
    "Created",
    "Published",
    "Recalled",
    "Completed"
  ]

  constructor(private service: SurveyTemplateService, private route: ActivatedRoute, private location: Location) { }

  ngOnInit() {
    let editorOptions = { showEmbededSurveyTab: false, showOptions: false, showJSONEditorTab: false, generateValidJSON: true, showTestSurveyTab: false };
    this.editor = new SurveyEditor.SurveyEditor('surveyEditorContainer', editorOptions);

    this.route.params.subscribe((params: Params) => {
      let id = params['id'];
      
      if(!!id){
        this.surveyTemplateId = id;

        this.service.getById(id).then(res => {
          console.log(res);
          this.surveyTemplate = res;
          this.title = res.title;

          this.surveyTemplateText = JSON.stringify(res);
          this.editor.text = this.surveyTemplateText;      
        });
      }
    });

    this.editor.saveSurveyFunc = this.saveMySurvey; 
  }

  saveMySurvey = () => {    
    var surveyTemplateObj = JSON.parse(this.editor.text) as SurveyTemplate;

    surveyTemplateObj.organisationKey = this.surveyTemplate.organisationKey;
    surveyTemplateObj.status = this.surveyTemplate.status;
    surveyTemplateObj.version = this.surveyTemplate.version;

    if(!!this.surveyTemplateId){
      surveyTemplateObj.idStr = this.surveyTemplate.idStr;
      this.service.update(this.surveyTemplateId, surveyTemplateObj).then(() => {
        this.goBack();
      })
    }
    else {
      this.service.create(surveyTemplateObj).then(() =>{
        this.goBack();
      });   
    } 
  }

  goBack(): void {
    this.location.back();
  }

}
