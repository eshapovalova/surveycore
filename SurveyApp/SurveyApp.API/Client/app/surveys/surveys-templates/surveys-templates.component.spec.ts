import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveysTemplatesComponent } from './surveys-templates.component';

describe('SurveysTemplatesComponent', () => {
  let component: SurveysTemplatesComponent;
  let fixture: ComponentFixture<SurveysTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
