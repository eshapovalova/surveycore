﻿import { NgModule } from '@angular/core';
import { CommonModule, NgFor } from '@angular/common';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';

import { SurveysRoutingModule } from './surveys-routing.module';
import { SurveysComponent } from './surveys.component';
import { SurveysListComponent } from './surveys-list/surveys-list.component';
import { SurveysEditComponent } from './surveys-edit/surveys-edit.component';

import { NgbDropdownModule, NgbModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2TableModule } from 'ng2-table/ng2-table';
import { ClipboardModule } from 'ngx-clipboard';
import { PaginationModule } from 'ngx-bootstrap';

import { HeaderComponent, SidebarComponent } from './../shared';
import { SurveysTemplatesComponent } from './surveys-templates/surveys-templates.component';
import { SurveysTemplatesEditComponent } from './surveys-templates-edit/surveys-templates-edit.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SurveysDetailsComponent } from './surveys-details/surveys-details.component';
import { SurveysDesignerComponent } from './surveys-designer/surveys-designer.component';
import { SurveysInstanceComponent } from './surveys-instance/surveys-instance.component';
import { ChooseParticipantComponent } from './choose-participant/choose-participant.component';
import { SurveysResponseDetailsComponent } from './surveys-response-details/surveys-response-details.component';
import { SurveysResponseComponent } from './surveys-response/surveys-response.component';

@NgModule({
  imports: [
    CommonModule,
    NgbDropdownModule.forRoot(),
    NgbModule.forRoot(),
    NgbTooltipModule.forRoot(),
    SurveysRoutingModule,
    TranslateModule,
    Ng2TableModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    PaginationModule
  ],
  declarations: [
      SurveysComponent,
      SurveysListComponent,
      HeaderComponent,
      SidebarComponent,
      SurveysEditComponent,
      SurveysTemplatesComponent,
      SurveysTemplatesEditComponent,
      DashboardComponent,
      SurveysDetailsComponent,
      SurveysDesignerComponent,
      SurveysInstanceComponent,
      ChooseParticipantComponent,
      SurveysResponseDetailsComponent,
      SurveysResponseComponent
  ]
})
export class SurveysModule { }
