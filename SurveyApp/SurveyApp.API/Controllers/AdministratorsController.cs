﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/administrators")]
    public class AdministratorsController : Controller
    {
        private readonly IAdministratorService _service;

        public AdministratorsController(IAdministratorService service)
        {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _service.GetAll();

            return Ok(items);
        }

        [HttpGet("{id}", Name ="GetAdministrator")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await _service.GetById(id);

            return Ok(item);
        }

        [HttpPost(Name = "CreateNewAdministrator")]
        public async Task<IActionResult> Post([FromBody] Administrator entity)
        {
            if(entity == null)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            var newId = await _service.Create(entity);

            if (string.IsNullOrEmpty(newId))
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Ok(newId);
        }

        [HttpPut("{id}", Name = "PatchAdministrator")]
        public async Task<IActionResult> Put(string id, [FromBody] Administrator entity)
        {
            if (entity == null)
                return StatusCode((int)HttpStatusCode.InternalServerError, "entity is null");

            await _service.Update(id, entity);

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteAdministrator")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _service.Delete(id);

            return result ? Ok(true) : Ok(false);
        }

    }
}