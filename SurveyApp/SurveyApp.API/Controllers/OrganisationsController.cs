﻿using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using System.Collections.Generic;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/organisations")]
    public class OrganisationsController : Controller
    {
        private readonly IOrganisationService _service;

        public OrganisationsController(IOrganisationService service)
        {
            _service = service;
        }

        [HttpGet(Name = "Get All")]
        public async Task<IActionResult> Get()
        {
            var items = await _service.GetAll();

            return Ok(items);
        }

        //[HttpGet(Name = "Get All")]
        //public async Task<IActionResult> Get(string filterName = null, string filterValue = null)
        //{
        //    IEnumerable<Organisation> items = new List<Organisation>();

        //    if(!string.IsNullOrEmpty(filterName) && !string.IsNullOrEmpty(filterValue))
        //    {
        //        items = await _service.GetByFilter(filterName, filterValue);
        //    }
        //    else
        //    {
        //        items = await _service.GetAll();
        //    }

        //    return Ok(items);
        //}

        [HttpGet("{id}", Name = "GetOrganisation")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await _service.GetById(id);

            return Ok(item);
        }

        [HttpPost(Name = "CreateNewOrganisation")]
        public async Task<IActionResult> Post([FromBody] Organisation entity)
        {
            if (entity == null)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            var newId = await _service.Create(entity);

            if (string.IsNullOrEmpty(newId))
            {
                return StatusCode((int)HttpStatusCode.InternalServerError);
            }

            return Ok(newId);
        }

        [HttpPut("{id}", Name = "PutOrganisation")]
        public async Task<IActionResult> Put(string id, [FromBody] Organisation entity)
        {
            if (entity == null)
                return StatusCode((int)HttpStatusCode.InternalServerError, "entity is null");

            await _service.Update(id, entity);

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteOrganisation")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _service.Delete(id);

            return result ? Ok(true) : Ok(false);
        }
    }
}