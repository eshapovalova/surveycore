using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/participants")]
    public class ParticipantsController : Controller
    {
        private readonly IParticipantService _service;

        public ParticipantsController(IParticipantService service)
        {
            _service = service;
        }

        [HttpGet(Name = "GetAllParticipants")]
        public async Task<IActionResult> Get()
        {
            var items = await _service.GetAll();

            return Ok(items);
        }
        
        [HttpGet("{id}", Name="GetParticipant")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await _service.GetById(id);
            return Ok(item);
        }

        [HttpPost(Name = "CreateNewParticipant")]
        public async Task<IActionResult> Post([FromBody] Participant entity)
        {
            if(entity == null) 
                return StatusCode((int)HttpStatusCode.InternalServerError);

            var newId = await _service.Create(entity);

            if(string.IsNullOrEmpty(newId))
                return StatusCode((int)HttpStatusCode.InternalServerError);

            return Ok(newId);
        }

        [HttpPut("{id}", Name = "UpdateParticipant")]
        public async Task<IActionResult> Put(string id, [FromBody] Participant entity)
        {
            if(entity == null)
                 return StatusCode((int)HttpStatusCode.InternalServerError);

            await _service.Update(id, entity);

            return Ok();
        }

        [HttpDelete("{id}", Name = "DeleteParticipant")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _service.Delete(id);

            return result ? Ok(true) : Ok(false);
        }
    }
}