﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using SurveyApp.API.ViewModels;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/surveys")]
    public class SurveysController : Controller
    {
        protected readonly ISurveyService _service;
        protected readonly ISurveyTemplateService _surveyTemplateService;
        protected readonly ISurveyResponseService _surveyResultService;

        public SurveysController(ISurveyService service, ISurveyTemplateService surveyTemplateService, ISurveyResponseService surveyResultService)
        {
            _service = service;
            _surveyTemplateService = surveyTemplateService;
            _surveyResultService = surveyResultService;
        }

        /// <summary>
        /// Get All surveys
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _service.LoadSurveyList();
            return Ok(items);
        }

        /// <summary>
        /// Get survey info for preview
        /// </summary>
        /// <param name="id">Object Id from MongoDb</param>
        /// <returns>Survey object</returns>
        [HttpGet("{id}", Name = "GetSurveyDetails")]
        public async Task<IActionResult> GetSurvey(string id)
        {
            var item = await _service.GetSurveyInfo(id);

            return Ok(item);
        }

        /// <summary>
        /// Update survey details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="survey"></param>
        /// <returns></returns>
        [HttpPut("{id}", Name = "UpdateSurvey")]
        public async Task<IActionResult> Put(string id, [FromBody] Survey survey)
        {
            if (survey == null)
                return StatusCode((int) StatusCodes.Status500InternalServerError, "requrest body is empty");

            survey.Id = ObjectId.Parse(id);

            await _service.Update(survey);

            return Ok();
        }

        [HttpPut("designer/{id}", Name="UpdateSurveyFromDesigner")]
        public async Task<IActionResult> UpdateSurveyDesigner(string id, [FromBody] SurveyDesignUpdateViewModel viewModel)
        {
            if(viewModel == null)
                return StatusCode((int) StatusCodes.Status500InternalServerError, "requrest body is empty");

            await _service.UpdateSurveyDesigner(id, viewModel.Title, viewModel.Pages);

            return Ok();
        }

        //[HttpGet("results", Name = "GetResults")]
        //public async Task<IActionResult> GetResults()
        //{
        //    var items = await _surveyResultService.GetResults();
        //    return Ok(items);
        //}

        //[HttpGet("result/{id}", Name = "GetResult")]
        //public async Task<IActionResult> GetResult(string id)
        //{
        //    var item = await _surveyResultService.GetResult(id);

        //    return Ok(item);
        //}

        ///// <summary>
        ///// Create survey function
        ///// </summary>
        ///// <param name="name"></param>
        ///// <returns></returns>
        //[HttpPost(Name = "CreateSurvey")]
        //public async Task<IActionResult> Post(SurveyTemplate template)
        //{
        //    var survey = new Survey {Title = template.Title, Pages = template.Pages};

        //    var newSurveyId = await _service.CreateSurvey(survey);

        //    return Ok(newSurveyId);
        //}

        //[HttpPost("delete", Name="DeleteSurvey")]
        //public async Task<IActionResult> Delete(string id)
        //{
        //    var count = await _service.DeleteSurvey(id);

        //    return count > 0 ? Ok() : StatusCode(StatusCodes.Status500InternalServerError);
        //}

        //[HttpPost("result", Name = "SaveSurveyResult"), Produces("application/json")]
        //public async Task<IActionResult> SaveResult([FromBody]SurveyResultViewModel viewModel)
        //{
        //    var newId = await _surveyResultService.SaveSurveyResult(viewModel.SurveyId, viewModel.SurveyResultId, viewModel.Result);

        //    return Ok(newId);
        //}
    }
}