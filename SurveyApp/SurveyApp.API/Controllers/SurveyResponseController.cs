﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json.Linq;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/SurveyResponse")]
    public class SurveyResponseController : Controller
    {
        private readonly ISurveyResponseService _service;

        public SurveyResponseController(ISurveyResponseService service)
        {
            _service = service;
        }

        [HttpGet(Name = "GetAllSurveyResponses")]
        public async Task<IActionResult> Get()
        {
            var items = await _service.GetAll();

            return Ok(items);
        }

        [HttpGet("{id}", Name = "GetSurveyResponse")]
        public async Task<IActionResult> Get(string id)
        {
            var item = await _service.GetById(id);

            return Ok(item);
        }

        [HttpGet("user/{id}", Name = "GetUserResponses")]
        public async Task<IActionResult> GetUserResponses(string id)
        {
            var items = await _service.GetByUserId(id);

            return Ok(items);
        }

        [HttpPost(Name = "SaveResponse")]
        public async Task<IActionResult> Post([FromBody] SurveyResponse response)
        {
            if (response == null)
                return StatusCode((int)StatusCodes.Status500InternalServerError);

            var newId = await _service.Create(response);

            return Ok(newId);
        }

        [HttpPut("{id}", Name = "UpdateResponse")]
        public async Task<IActionResult> Put(string id, [FromBody] SurveyResponse response)
        {
            if(response == null)
                return StatusCode((int)StatusCodes.Status500InternalServerError);

            response.Id = ObjectId.Parse(id);
            await _service.Update(response);

            return Ok(true);
        }

        [HttpPut("fill/{id}", Name = "UpdateUserResponse")]
        public async Task<IActionResult> Put(string id, [FromBody] JObject response)
        {
            if (response == null)
                return StatusCode((int)StatusCodes.Status500InternalServerError);

            await _service.Update(id, response);

            return Ok();
        }
    }
}