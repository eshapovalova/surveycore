﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using Newtonsoft.Json;
using SurveyApp.API.ViewModels;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;

namespace SurveyApp.API.Controllers
{
    [Produces("application/json")]
    [Route("api/templates")]
    public class SurveyTemplateController : Controller
    {
        protected readonly ISurveyTemplateService _service;

        public SurveyTemplateController(ISurveyTemplateService service)
        {
            _service = service;
        }

        /// <summary>
        /// Get survey templates
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var items = await _service.LoadSurveyTemplates();

            return Ok(items);
        }

        /// <summary>
        /// Get template 
        /// </summary>
        /// <param name="id">Identifier</param>
        /// <returns>Survey Template</returns>
        [HttpGet("{id}", Name = "GetTemplateDetails")]
        public async Task<IActionResult> GetTemplate(string id)
        {
            var item = await _service.LoadSurveyTemplate(id);

            return Ok(item);
        }

        [HttpPost(Name = "CreateTemplate")]
        public async Task<IActionResult> Post([FromBody]SurveyTemplate template)
        {
            if (template == null)
                return StatusCode((int) StatusCodes.Status500InternalServerError);


            var newId = await _service.CreateSurveyTemplate(template);

            return Ok(newId);
        }
        
        [HttpPost("use/{id}", Name = "UseTemplate")]
        public async Task<IActionResult> UseTemplate(string id)
        {
            var newId = await _service.UseTemplate(id);

            if (string.IsNullOrEmpty(newId))
                return StatusCode((int)StatusCodes.Status500InternalServerError);

            return Ok(newId);
        }

        [HttpPut("{id}", Name = "UpdateTemplate")]
        public async Task<IActionResult> Put(string id, [FromBody] SurveyTemplate template)
        {
            if (template == null)
                return StatusCode((int)StatusCodes.Status500InternalServerError);

            template.Id = ObjectId.Parse(id);
            await _service.UpdateSurveyTemplate(template);

            return Ok(true);
        }

        [HttpDelete("{id}", Name = "DeleteTemplate")]
        public async Task<IActionResult> Delete(string id)
        {
            var result = await _service.DeleteSurveyTemplate(id);

            return result ? Ok(true) : Ok(false);
        }
    }
}