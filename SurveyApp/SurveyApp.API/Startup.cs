﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using MongoDB.Bson.Serialization;
using Swashbuckle.AspNetCore.Swagger;
using SurveyApp.Core.Domain;
using SurveyApp.Storage.MongoDb.Repository.Base;
using SurveyApp.Storage.MongoDb.Repository.Repository;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Core.Services;

namespace SurveyApp.API
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfiguration>(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsDevPolicy", builder =>
                {
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader()
                        .AllowCredentials();
                });

            });

            services.AddDistributedMemoryCache();

            SetTransient(services);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Survey App API", Version = "v1" });
                
                // enable XML comments: 
                var filePath = Path.Combine(PlatformServices.Default.Application.ApplicationBasePath, "SurveyApp.API.xml");
                c.IncludeXmlComments(filePath);
            });

            // Add framework services.
            services.AddMvc(options =>
            {
                options.RespectBrowserAcceptHeader = true;

                options.Filters.Add(new ProducesAttribute("application/json"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddFile("Logs/myapp-{Date}.txt");
            loggerFactory.AddDebug();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Survey App API v1");
                //c.ShowJsonEditor();
                c.ShowRequestHeaders();
            });

            app.Use(async (context, next) => {
                await next();
                if (context.Response.StatusCode == 404 &&
                    !Path.HasExtension(context.Request.Path.Value) &&
                    !context.Request.Path.Value.StartsWith("/api/"))
                {
                    context.Request.Path = "/index.html";
                    await next();
                }
            });

            app.UseCors("CorsDevPolicy");

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseMvc();
        }

        private void SetTransient(IServiceCollection services)
        {
            BsonClassMap.RegisterClassMap<Survey>();

            var mongoSettins = Configuration["MondoBb:ConnectionString"];
            
            // repository settings
            services.AddTransient<IRepository<Survey>>(rep => new Repository<Survey>(mongoSettins));
            services.AddTransient<IRepository<SurveyTemplate>>(rep => new Repository<SurveyTemplate>(mongoSettins));
            services.AddTransient<IRepository<SurveyResult>>(rep => new Repository<SurveyResult>(mongoSettins));
            services.AddTransient<IRepository<Organisation>>(rep => new Repository<Organisation>(mongoSettins));
            services.AddTransient<IRepository<Administrator>>(rep => new Repository<Administrator>(mongoSettins));
            services.AddTransient<IRepository<Participant>>(rep => new Repository<Participant>(mongoSettins));
            services.AddTransient<IRepository<SurveyResponse>>(rep => new Repository<SurveyResponse>(mongoSettins));
            
            // services settings
            services.AddTransient<ISurveyService, SurveyService>();
            services.AddTransient<ISurveyTemplateService, SurveyTemplateService>();
            services.AddTransient<ISurveyResponseService, SurveyResponseService>();
            services.AddTransient<IOrganisationService, OrganisationService>();
            services.AddTransient<IAdministratorService, AdministratorService>();
            services.AddTransient<IParticipantService, ParticipantService>();
        }
    }
}
