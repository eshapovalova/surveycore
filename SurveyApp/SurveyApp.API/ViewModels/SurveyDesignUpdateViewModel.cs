using System.Collections.Generic;
using SurveyApp.Core.Domain;

namespace SurveyApp.API.ViewModels
{
    public class SurveyDesignUpdateViewModel
    {
        public string Title { get; set; }
        
        public IEnumerable<SurveyPage> Pages { get; set; }
    }
}