﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace SurveyApp.API.ViewModels
{
    public class SurveyResultViewModel
    {
        public string SurveyResultId { get; set; }

        public string SurveyId { get; set; }

        public JObject Result { get; set; }
    }
}
