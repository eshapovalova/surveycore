﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;

namespace SurveyApp.API.ViewModels
{
    public class TempViewModel
    {
        public Organisation Organisation { get; set; }
    }
}
