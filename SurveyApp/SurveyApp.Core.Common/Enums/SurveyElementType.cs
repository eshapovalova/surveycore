﻿using System;

namespace SurveyApp.Core.Common.Enums
{
    public enum SurveyElementType
    {
        Text,
        Radiogroup,
        Panel,
        Multipletext,
        Matrixdynamic,
        Matrixdropdown,
        Matrix,
        File,
        Html,
        Rating,
        Dropdown,
        Comment,
        Checkbox
    }
}