﻿using System.ComponentModel.DataAnnotations;

namespace SurveyApp.Core.Domain
{
    public class Address
    {
        [Required]
        public string AddressType { get; set; }

        public string AddressLine1 { get; set; }
        
        public string AddressLine2 { get; set; }
        
        public string AddressLine3 { get; set; }
        
        [Required]
        public string City { get; set; }
        
        [Required]
        public string PostCode { get; set; }
        
        [Required]
        public string State { get; set; }
        
        [Required]
        public string Country { get; set; }
    }
}