﻿using MongoDB.Bson.Serialization.Attributes;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("administrators")]
    public class Administrator : Entity
    {
        [BsonElement("administratorKey")]
        public string Key { get; set; }
        
        [BsonElement("organisation")]
        public string OrganisationKey { get; set; }

        [BsonElement("firstName")]
        public string FirstName { get; set; }
        
        [BsonElement("lastName")]
        public string LastName { get; set; }
        
        [BsonElement("address")]
        public Address Address { get; set; }
        
        [BsonElement("Phone")]
        public string Phone { get; set; }
        
        [BsonElement("Email")]
        public string Email { get; set; }
        
        
    }
}