﻿using MongoDB.Bson.Serialization.Attributes;
using SurveyApp.Core.Common.Helpers;
 using SurveyApp.Storage.MongoDb.Repository.Base;
 
 namespace SurveyApp.Core.Domain
 {
     [CollectionName("organisations")]
     public class Organisation : Entity
     {
         [BsonElement("key")]
         public string Key { get; set; }
         
         [BsonElement("legalName")]
         public string LegalName { get; set; }
         
         [BsonElement("Address")]
         public Address Address { get; set; }

         public string Phone { get; set; }

         public string Email { get; set; }
     }
 }