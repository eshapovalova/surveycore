﻿using MongoDB.Bson.Serialization.Attributes;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("participants")]
    public class Participant : Entity
    {
        [BsonElement("participantKey")]
        public string Key { get; set; }
        
        // TODO: Need to be clarify
        [BsonElement("identityKey")]
        public string IdentityKey { get; set; }
        
        // TODO: Need to be clarify
        [BsonElement("clientKey")]
        public string ClientKey { get; set; }

        [BsonElement("firstName")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        public string LastName { get; set; }
        
        [BsonElement("address")]
        public Address Address { get; set; }
        
        [BsonElement("phone")]
        public string Phone { get; set; }
        
        [BsonElement("email")]
        public string Email { get; set; }
    }
}