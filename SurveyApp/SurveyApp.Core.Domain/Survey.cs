﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("surveys")]
    public class Survey : Entity
    {
        public Survey()
        {
            Pages = new List<SurveyPage>();
        }

        [BsonElement("organisationKey")]
        public string OrganisationKey { get; set; }
        
        [BsonElement("surveyKey")]
        public string SurveyKey { get; set; }
        
        /// <summary>
        /// SurveyName
        /// Need to stay "title" cause of surveyjs requirement about object which can be used
        /// </summary>
        [BsonElement("title")]
        [JsonProperty("title")]
        public string Title { get; set; }

        [BsonElement("surveyStatus")]
        public string SurveyStatus { get; set; }

        [BsonElement("releasedDate")]
        public DateTime ReleasedDate { get; set; }
        
        [BsonElement("completionDate")]
        public DateTime CompletionDate { get; set; }
        
        [BsonElement("participantKeys")]
        public IEnumerable<string> ParticipantKeys { get; set; }
        
        [BsonElement("surveyComments")]
        public string SurveyComments { get; set; }
        
        [BsonElement("surveyTemplateVersion")]
        public string SurveyTemplateVersion { get; set; }
        
        [BsonElement("pages")]
        [JsonProperty("pages")]
        public IEnumerable<SurveyPage> Pages { get; set; }
    }
}