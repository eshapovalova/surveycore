﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SurveyApp.Core.Domain
{
    public class SurveyElement
    {
        public SurveyElement()
        {
            Items = new List<SurveyItem>();
        }

        [BsonElement("type")]
        public string Type { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("title")]
        public string Title { get; set; }

        [BsonElement("isRequired")]
        public bool IsRequired { get; set; }

        [BsonElement("choices")]
        public IList<object> Choices { get; set; }

        [BsonElement("items")]
        public IEnumerable<SurveyItem> Items { get; set; }

        [BsonElement("columns")]
        public IList<object> Columns { get; set; }

        [BsonElement("rows")]
        public IList<string> Rows { get; set; }

        [BsonElement("visible")]
        public bool? Visible { get; set; }

        [BsonElement("visibleIf")]
        public string VisibleIf { get; set; }

        [BsonElement("hasOther")]
        public bool? HasOther { get; set; }
    }
}
