﻿using MongoDB.Bson.Serialization.Attributes;

namespace SurveyApp.Core.Domain
{
    public class SurveyItem
    {
        [BsonElement("name")]
        public string Name { get; set; }
    }
}
