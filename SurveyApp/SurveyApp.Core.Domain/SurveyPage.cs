﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;

namespace SurveyApp.Core.Domain
{
    public class SurveyPage
    {
        public SurveyPage()
        {
            Elements = new List<SurveyElement>();
        }

        [BsonElement("elements")]
        public IEnumerable<SurveyElement> Elements { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }
    }
}
