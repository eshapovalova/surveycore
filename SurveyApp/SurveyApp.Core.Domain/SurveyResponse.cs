﻿using System;
using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("surveyResponses")]
    public class SurveyResponse : Entity
    {
        [BsonElement("surveyResponseKey")]
        public string SurveyResponseKey { get; set; }

        [BsonElement("surveyKey")]
        public string SurveyKey { get; set; }

        [BsonElement("participantKey")]
        public string ParticipantKey { get; set; }

        [BsonElement("progress")]
        public string Progress { get; set; }

        [BsonElement("openedDate")]
        public DateTime OpenedDate { get; set; }

        [BsonElement("completionDate")]
        public DateTime CompletionDate { get; set; }

        [BsonElement("Response")]
        public string Response { get; set; }

        [BsonElement("responseComments")]
        public string ResponseComments { get; set; }

        [BsonIgnore]
        public string Title { get; set; }

        [BsonIgnore]
        public IEnumerable<SurveyPage> Pages { get; set; }
    }
}
