﻿using System;
using System.Collections.Generic;
using System.Text;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json.Linq;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("results")]
    public class SurveyResult : Entity
    {
        [BsonElement("result")]
        public string Result { get; set; }

        [BsonElement("surveyId")]
        public ObjectId SurveyId { get; set; }

        [BsonIgnore]
        public string SurveyIdStr => SurveyId.ToString();

        [BsonIgnore]
        public string Title { get; set; }

        [BsonIgnore]
        public IEnumerable<SurveyPage> Pages { get; set; }

        [BsonIgnore]
        public string IdStr => Id.ToString();
    }
}
