﻿using System.Collections.Generic;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Domain
{
    [CollectionName("surveyTemplates")]
    public class SurveyTemplate : Entity
    {
        public SurveyTemplate()
        {
            if (string.IsNullOrEmpty(this.OrganisationKey))
                this.OrganisationKey = string.Empty;

            if (string.IsNullOrEmpty(Status))
                this.Status = "new";

            if (string.IsNullOrEmpty(Version))
                this.Version = "0.0.000";
        }

        /// <summary>
        /// The Survey Template Key
        /// </summary>
        [BsonElement("title")]
        public string Title { get; set; }

        [BsonDefaultValue("")]
        [BsonElement("organisationKey")]
        public string OrganisationKey { get; set; }
        
        // TODO: Need to be clarify
        [BsonDefaultValue("new")]
        [BsonElement("surveyTemplateStatus")]
        public string Status { get; set; }
        
        // TODO: Need to be clarify
        [BsonElement("surveyTemplateVersion")]
        [BsonDefaultValue("0.0.00")]
        public string Version { get; set; }
        
        [BsonElement("pages")]
        public IEnumerable<SurveyPage> Pages { get; set; }
    }
}