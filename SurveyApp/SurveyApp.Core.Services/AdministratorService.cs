using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Services
{
    public class AdministratorService : IAdministratorService
    {
        private readonly IRepository<Administrator> _repository;

        public AdministratorService(IRepository<Administrator> repository)
        {
            _repository = repository;
        }

        public async Task<string> Create(Administrator entity)
        {
             var newest = await _repository.Create(entity);

            return newest.ToString();
        }

        public async Task<bool> Delete(string id)
        {
            var count = await _repository.Delete(id);

            return count > 0;
        }

        public async Task<IEnumerable<Administrator>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<Administrator> GetById(string id)
        {
            return await _repository.GetById(ObjectId.Parse(id));
        }

        public async Task Update(string id, Administrator entity)
        {
            entity.Id = ObjectId.Parse(id);
            await _repository.Update(entity);
        }
    }
}