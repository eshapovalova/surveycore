﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Storage.MongoDb.Repository.Base;
using MongoDB.Driver;

namespace SurveyApp.Core.Services
{
    public class OrganisationService : IOrganisationService
    {
        private readonly IRepository<Organisation> _repository;

        public OrganisationService(IRepository<Organisation> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<Organisation>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<IEnumerable<Organisation>> GetByFilter(string filterName, string filterValue)
        {
            var filter = Builders<Organisation>.Filter.Text(filterValue);
            var projection = Builders<Organisation>.Projection.MetaTextScore(filterName);
            
            var items = await _repository.GetAll(filter, projection);

            return items;
        }

        public async Task<Organisation> GetById(string id)
        {
            return await _repository.GetById(ObjectId.Parse(id));
        }

        public async Task<string> Create(Organisation entity)
        {
            var newest = await _repository.Create(entity);

            return newest.ToString();
        }

        public async Task Update(string id, Organisation entity)
        {
            entity.Id = ObjectId.Parse(id);
            await _repository.Update(entity);
        }

        public async Task<bool> Delete(string id)
        {
            var count = await _repository.Delete(id);

            return count > 0;
        }       
    }
}
