using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using MongoDB.Bson;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Services
{
    public class ParticipantService : IParticipantService
    {
        private readonly IRepository<Participant> _repository;

        public ParticipantService(IRepository<Participant> repository)
        {
            _repository = repository;
        }

        public async Task<string> Create(Participant entity)
        {
            var newId = await _repository.Create(entity);

            return newId.ToString();
        }

        public async Task<bool> Delete(string id)
        {
            var count = await _repository.Delete(id);

            return count > 0;
        }

        public async Task<IEnumerable<Participant>> GetAll()
        {
            return await _repository.GetAll();
        }

        public async Task<Participant> GetById(string id)
        {
            return await _repository.GetById(ObjectId.Parse(id));
        }

        public async Task Update(string id, Participant entity)
        {
            entity.Id = ObjectId.Parse(id);

            await _repository.Update(entity);
        }
    }
}