﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Services
{
    public class SurveyResponseService : ISurveyResponseService
    {
        private readonly IRepository<SurveyResponse> _repository;
        private readonly IRepository<Survey> _surveyRepository;

        public SurveyResponseService(IRepository<SurveyResponse> repository, IRepository<Survey> surveyRepository)
        {
            _repository = repository;
            _surveyRepository = surveyRepository;
        }

        public async Task<IEnumerable<SurveyResponse>> GetAll()
        {
            var items = await _repository.GetAll();

            foreach (var response in items)
            {
                var surveyInfo = await _surveyRepository.GetById(response.Id);
                if (surveyInfo != null)
                {
                    response.Title = surveyInfo.Title;
                }
            }

            return items;
        }

        public async Task<SurveyResponse> GetById(string id)
        {
            var surveyResponse = await _repository.GetById(ObjectId.Parse(id));

            if (surveyResponse != null)
            {
                var surveyInfo = await _surveyRepository.GetById(ObjectId.Parse(surveyResponse.SurveyKey));
                if (surveyInfo != null)
                {
                    surveyResponse.Title = surveyInfo.Title;
                    surveyResponse.Pages = surveyInfo.Pages;
                }
            }

            return surveyResponse;
        }

        public async Task<IEnumerable<SurveyResponse>> GetBySurveyKey(string surveyKey)
        {
            var filter = Builders<SurveyResponse>.Filter.Eq(x => x.SurveyKey, surveyKey);

            var items = await _repository.GetAll(filter);

            foreach (var item in items)
            {
                var surveyInfo = await _surveyRepository.GetById(item.Id);
                if (surveyInfo != null)
                {
                    item.Title = surveyInfo.Title;
                }
            }

            return items;
        }

        public async Task<IEnumerable<SurveyResponse>> GetByUserId(string userKey)
        {
            var filter = Builders<SurveyResponse>.Filter.Eq(x => x.ParticipantKey, userKey);

            var items = await _repository.GetAll(filter);

            foreach (var item in items)
            {
                var surveyInfo = await _surveyRepository.GetById(ObjectId.Parse(item.SurveyKey));
                if (surveyInfo != null)
                {
                    item.Title = surveyInfo.Title;
                }
            }

            return items;
        }

        public async Task<string> Create(SurveyResponse response)
        {
            var newId = await _repository.Create(response);

            return newId.ToString();
        }

        public async Task Update(string id, JObject result)
        {
            var item = await _repository.GetById(ObjectId.Parse(id));

            if (item != null)
            {
                item.Response = result.ToString();
                await _repository.Update(item);
            }
        }

        public async Task Update(SurveyResponse response)
        {
            await _repository.Update(response);
        }
    }
}
