﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Services
{
    public class SurveyService : ISurveyService
    {
        private readonly IRepository<Survey> _repository;

        public SurveyService(IRepository<Survey> repository)
        {
            _repository = repository;
        }

        public async Task CreateSurvey(string name)
        {
            var survey = new Survey {Title = name};

            await _repository.Create(survey);
        }

        public async Task<string> CreateSurvey(Survey survey)
        {
            var newSurveyId = await _repository.Create(survey);

            return newSurveyId.ToString();
        }

        public async Task<IEnumerable> LoadSurveyList()
        {
            var items = await _repository.GetAll();

            return items;
        }

        public async Task<Survey> GetSurveyInfo(string id)
        {
            var item = await _repository.GetById(ObjectId.Parse(id));

            return item;
        }

        public async Task<long> DeleteSurvey(string id)
        {
            var count = await _repository.Delete(id);

            return count;
        }

        public async Task Update(Survey survey)
        {
            await _repository.Update(survey);
        }

        public async Task UpdateSurveyDesigner(string id, string title, IEnumerable<SurveyPage> pages)
        {
            var survey = await GetSurveyInfo(id);

            if(survey != null)
            {
                survey.Title = title;
                survey.Pages = pages;

                await _repository.Update(survey);
            }
        }
    }
}