﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurveyApp.Core.Domain;
using SurveyApp.Infrastructure.Services;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Core.Services
{
    public class SurveyTemplateService : ISurveyTemplateService
    {
        public readonly IRepository<SurveyTemplate> _repository;
        public readonly IRepository<Survey> _surveyRepository;

        public SurveyTemplateService(IRepository<SurveyTemplate> repository, IRepository<Survey> surveyRepository)
        {
            _repository = repository;
            _surveyRepository = surveyRepository;
        }

        public async Task<IEnumerable> LoadSurveyTemplates()
        {
            var items = await _repository.GetAll();
            
            return items;
        }

        public async Task<SurveyTemplate> LoadSurveyTemplate(string objectId)
        {
            var item = await _repository.GetById(ObjectId.Parse(objectId));

            return item;
        }

        public async Task<string> CreateSurveyTemplate(SurveyTemplate template)
        {
            var result = await _repository.Create(template);

            return result.ToString();
        }

        public async Task UpdateSurveyTemplate(SurveyTemplate template)
        {
            await _repository.Update(template);
        }

        public async Task<bool> DeleteSurveyTemplate(string objectId)
        {
            var count = await _repository.Delete(objectId);

            return count > 0;
        }

        public async Task<string> CopyTemplate(string toCopyObjecId)
        {
            var existObject = await _repository.GetById(ObjectId.Parse(toCopyObjecId));

            var newObject = new SurveyTemplate { Title = $"Copy of {existObject.Title}", Pages = existObject.Pages};

            var newObjectId = await _repository.Create(newObject);

            return newObjectId.ToString();
        }

        public async Task<string> UseTemplate(string templateId)
        {
            var template = await LoadSurveyTemplate(templateId);

            if (template != null)
            {
                var newSurvey = new Survey
                {
                    Title = template.Title,
                    Pages = template.Pages,
                    OrganisationKey = template.OrganisationKey,
                    ReleasedDate = DateTime.UtcNow,
                    SurveyStatus = "new",
                    SurveyKey = template.Title.ToLower().Replace(' ', '_'),
                    SurveyTemplateVersion = template.Version
                };

                var newSurveyId = await _surveyRepository.Create(newSurvey);

                return newSurveyId.ToString();
            }

            return string.Empty;
        }
    }
}
