﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;

namespace SurveyApp.Infrastructure.Domain
{
    [CollectionName("surveys")]
    public interface ISurvey : IEntity
    {
        [JsonProperty("pages")]
        IEnumerable<ISurveyPage> SurveyPages { get; set; }

        [JsonProperty("title")]
        string Title { get; set; }

        DateTime CreatedAt { get; set; }
        
        DateTime UpdatedAt { get; set; }
    }
}