﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace SurveyApp.Infrastructure.Domain
{
    public interface ISurveyElement
    {
        [JsonProperty("type")]
        string Type { get; set; }

        [JsonProperty("name")]
        string Name { get; set; }

        [JsonProperty("title")]
        string Title { get; set; }

        [JsonProperty("isRequired")]
        bool IsRequired { get; set; }

        [JsonProperty("choices")]
        IList<object> Choices { get; set; }

        [JsonProperty("items")]
        IEnumerable<ISurveyItem> Items { get; set; }

        [JsonProperty("columns")]
        IList<object> Columns { get; set; }

        [JsonProperty("rows")]
        IList<string> Rows { get; set; }

        [JsonProperty("visible")]
        bool? Visible { get; set; }

        [JsonProperty("visibleIf")]
        string VisibleIf { get; set; }

        [JsonProperty("hasOther")]
        bool? HasOther { get; set; }
    }
}