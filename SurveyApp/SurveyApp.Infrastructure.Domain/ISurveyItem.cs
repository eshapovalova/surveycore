﻿using Newtonsoft.Json;

namespace SurveyApp.Infrastructure.Domain
{
    public interface ISurveyItem
    {
        [JsonProperty("name")]
        string Name { get; set; }
    }
}