﻿using System.Collections.Generic;
using Newtonsoft.Json;
using SurveyApp.Core.Common.Helpers;

namespace SurveyApp.Infrastructure.Domain
{
    public interface ISurveyPage
    {
        [JsonProperty("elements")]
        IEnumerable<ISurveyElement> Elements { get; set; }

        [JsonProperty("name")]
        string Name { get; set; }
    }
}