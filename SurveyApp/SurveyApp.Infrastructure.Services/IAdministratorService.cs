using System.Collections.Generic;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface IAdministratorService
    {
        Task<IEnumerable<Administrator>> GetAll();

        Task<Administrator> GetById(string id);

        Task<string> Create(Administrator entity);

        Task Update(string id, Administrator entity);

        Task<bool> Delete(string id);
    }
}