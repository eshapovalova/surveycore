﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface IOrganisationService
    {
        Task<IEnumerable<Organisation>> GetAll();

        Task<Organisation> GetById(string id);

        Task<string> Create(Organisation entity);

        Task Update(string id, Organisation entity);

        Task<bool> Delete(string id);

        Task<IEnumerable<Organisation>> GetByFilter(string filterName, string filterValue);
    }
}