using System.Collections.Generic;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface IParticipantService
    {
         Task<IEnumerable<Participant>> GetAll();

         Task<Participant> GetById(string id);

         Task<string> Create(Participant entity);

         Task Update(string id, Participant entity);

         Task<bool> Delete(string id);
    }
}