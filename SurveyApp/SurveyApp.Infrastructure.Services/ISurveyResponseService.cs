﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface ISurveyResponseService
    {
        Task<IEnumerable<SurveyResponse>> GetAll();

        Task<SurveyResponse> GetById(string id);

        Task<IEnumerable<SurveyResponse>> GetBySurveyKey(string surveyKey);
        Task<IEnumerable<SurveyResponse>> GetByUserId(string userKey);

        Task<string> Create(SurveyResponse response);

        Task Update(string id, JObject result);
        Task Update(SurveyResponse response);
    }
}