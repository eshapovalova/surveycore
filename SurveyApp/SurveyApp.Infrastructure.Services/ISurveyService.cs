﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface ISurveyService
    {
        Task CreateSurvey(string name);

        Task<string> CreateSurvey(Survey survey);

        Task<IEnumerable> LoadSurveyList();

        Task<Survey> GetSurveyInfo(string id);

        Task<long> DeleteSurvey(string id);

        Task Update(Survey survey);

        Task UpdateSurveyDesigner(string id, string viewModelTitle, IEnumerable<SurveyPage> viewModelPages);
    }
}
