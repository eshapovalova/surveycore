﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using SurveyApp.Core.Domain;

namespace SurveyApp.Infrastructure.Services
{
    public interface ISurveyTemplateService
    {
        Task<IEnumerable> LoadSurveyTemplates();

        Task<SurveyTemplate> LoadSurveyTemplate(string objectId);

        Task<string> CreateSurveyTemplate(SurveyTemplate template);

        Task UpdateSurveyTemplate(SurveyTemplate template);

        Task<bool> DeleteSurveyTemplate(string objectId);

        Task<string> CopyTemplate(string toCopyObjecId);

        Task<string> UseTemplate(string templateId);
    }
}