﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SurveyApp.Storage.MongoDb.Repository.Base
{
    public class Entity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId Id { get; set; }

        [BsonElement("createdAt")]
        public DateTime CreatedAt { get; set; }

        [BsonElement("updatedAt")]
        public DateTime UpdatedAt { get; set; }
        
        [BsonElement("createdBy")]
        public string CreatedBy { get; set; }
        
        [BsonElement("updatedBy")]
        public string UpdatedBy { get; set; }
        
        [BsonElement("isDeleted")]
        public bool IsDeleted { get; set; }

        [BsonIgnore]
        public string IdStr => Id.ToString();
    }
}