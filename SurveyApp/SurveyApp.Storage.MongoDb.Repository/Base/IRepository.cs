﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SurveyApp.Storage.MongoDb.Repository.Base
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<IEnumerable<TEntity>> GetAll(FilterDefinition<TEntity> filter = null, ProjectionDefinition<TEntity> projection = null);

        Task<IEnumerable<TEntity>> GetByQuery(Expression<Func<TEntity, bool>> expression);

        Task<TEntity> GetById(ObjectId key);

        Task<ObjectId> Create(TEntity entity);
        
        Task Update(TEntity entity);

        Task<long> Delete(string key);
    }
}