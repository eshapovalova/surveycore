﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using SurveyApp.Core.Common.Helpers;
using SurveyApp.Storage.MongoDb.Repository.Base;
using Newtonsoft.Json.Linq;

namespace SurveyApp.Storage.MongoDb.Repository.Repository
{
    public class Repository<T> : IRepository<T> where T : Entity 
    {
        private readonly Driver _driver;
        private readonly IMongoCollection<T> Collection;

        public Repository(Driver driver)
        {
            _driver = driver;
        }

        public Repository(string connectionString)
        {
            _driver = new Driver(connectionString);

            Type entiType = typeof(T);
            var collection = entiType.GetTypeInfo().GetCustomAttribute<CollectionNameAttribute>();

            if (collection != null)
            {
                Collection = _driver.Database.GetCollection<T>(collection.Name);
            }
        }
        
        public async Task<IEnumerable<T>> GetAll(FilterDefinition<T> filter = null, ProjectionDefinition<T> projection = null)
        {
            var items = new List<T>();

            if (filter == null)
            {
                filter = Builders<T>.Filter.Empty;
            }
            
            return await Collection.Find(filter).ToListAsync();
        }

        public async Task<IEnumerable<T>> GetByQuery(Expression<Func<T, bool>> expression)
        {
            var results = await Collection.FindAsync(expression);

            return results.ToList();
        }

        public async Task<T> GetById(ObjectId id)
        {
            var result = await Collection.FindAsync(e => e.Id == id);

            return await result.FirstOrDefaultAsync();
        }

        public async Task<ObjectId> Create(T entity)
        {
            entity.Id = ObjectId.GenerateNewId();
            entity.CreatedAt = DateTime.UtcNow;
            entity.UpdatedAt = DateTime.UtcNow;

            await Collection.InsertOneAsync(entity);

            return entity.Id;
        }
        
        public async Task Update(T entity)
        {
            entity.UpdatedAt = DateTime.UtcNow;

            await Collection.FindOneAndReplaceAsync(x => x.Id == entity.Id, entity);
        }
        
        public async Task<long> Delete(string key)
        {
            var result = await Collection.DeleteOneAsync(x => x.Id == ObjectId.Parse(key));

            return result.DeletedCount;
        }
    }
}